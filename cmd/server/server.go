package main

import (
	"gitlab.com/gppopoff/simplegrpcapplication/list"
	"gitlab.com/gppopoff/simplegrpcapplication/server"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

const port = ":9000"

func main() {

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	if len(os.Args) == 2 {
		list.RegisterShoppingListServer(grpcServer, server.NewFileShoppingListServer(os.Args[1]))
	} else {
		list.RegisterShoppingListServer(grpcServer, server.NewInMemShoppingListServer())
	}

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
