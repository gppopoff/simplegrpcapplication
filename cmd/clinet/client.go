package main

import (
	"gitlab.com/gppopoff/simplegrpcapplication/client"
	"gitlab.com/gppopoff/simplegrpcapplication/list"
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"time"
)

const address = "localhost:9000"

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	//c := list.NewGreeterClient(conn)
	shoppingListClient := list.NewShoppingListClient(conn)

	numOfCommand := 0
	for {
		sec := rand.Intn(10) + 1
		log.Printf("Waiting %v seconds", sec)
		time.Sleep(time.Duration(sec) * time.Second)

		command := rand.Intn(3)

		switch command {
		case 0:
			client.SendCreate(numOfCommand, shoppingListClient) // ne znam dali ne trqbva da podavam ShoppingListClient kato referenciq ???
		case 1:
			item := rand.Intn(numOfCommand)
			client.SendDelete(item, shoppingListClient)
		case 2:
			client.SendList(numOfCommand, shoppingListClient)
		}
		numOfCommand++
	}

}
