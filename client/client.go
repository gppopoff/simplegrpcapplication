package client

import (
	"context"
	"gitlab.com/gppopoff/simplegrpcapplication/list"
	"io"
	"log"
	"strconv"
)

func SendCreate(number int, client list.ShoppingListClient) {
	log.Println("Sending create request ...")
	r, err := client.Create(context.Background(), &list.Item{Name: strconv.Itoa(number)})
	if err != nil {
		log.Printf("error when calling Create :%s", err)
	}
	log.Println("Received respond with message: " + r.GetMessage())
}

func SendDelete(number int, client list.ShoppingListClient) {
	log.Println("Sending delete request ...")
	r, err := client.Delete(context.Background(), &list.Item{Name: strconv.Itoa(number)})
	if err != nil {
		log.Printf("error when calling Delete :%s", err)
	}
	log.Println(r.GetMessage())
	log.Println("Received respond with message: " + r.GetMessage())
}

func SendList(number int, client list.ShoppingListClient) {
	log.Println("Sending create request ...")
	stream, err := client.List(context.Background(), &list.Item{})
	if err != nil {
		log.Printf("error in request for listing iteam: %v", err)
	}
	log.Println("Received list ot items:")
	for {
		item, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("error in listing items: %v", err)
		}
		log.Println(item.GetName())
	}
}
