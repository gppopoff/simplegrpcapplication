package server

import (
	"context"
	"gitlab.com/gppopoff/simplegrpcapplication/list"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
	"log"
	"net"
	"testing"
)

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)

	server := grpc.NewServer()

	list.RegisterShoppingListServer(server, NewInMemShoppingListServer())

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func fileDialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)

	server := grpc.NewServer()

	list.RegisterShoppingListServer(server, NewFileShoppingListServer("test.txt"))

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func TestShoppingListServer_Create(t *testing.T) {
	tests := []struct {
		name     string
		itemName string
		res      *list.Message
		errMsg   string
	}{
		{
			"valid request for item",
			"Krusha",
			&list.Message{Message: "successful adding item Krusha in the list"},
			"",
		},
		{
			"valid request for item",
			"Topka",
			&list.Message{Message: "successful adding item Topka in the list"},
			"",
		},
		{
			"invalid request for item",
			"Krusha",
			&list.Message{Message: "unsuccessful adding item Krusha in the list"},
			"newItem already in the list",
		},
	}

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(), grpc.WithContextDialer(dialer()))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := list.NewShoppingListClient(conn)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &list.Item{Name: tt.itemName}

			response, err := client.Create(ctx, request)

			if response != nil {
				if response.GetMessage() != tt.res.GetMessage() {
					t.Error("response: expected", tt.res.GetMessage(), "received", response.GetMessage())
				}
			}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Message() != tt.errMsg {
						t.Error("error message: expected", tt.errMsg, "received", er.Message())
					}
				}
			}

		})
	}

}

func TestShoppingListServerWithFile_Create(t *testing.T) {
	tests := []struct {
		name     string
		itemName string
		res      *list.Message
		errMsg   string
	}{
		{
			"valid request for item",
			"Krusha",
			&list.Message{Message: "successful adding item Krusha in the list"},
			"",
		},
		{
			"valid request for item",
			"Topka",
			&list.Message{Message: "successful adding item Topka in the list"},
			"",
		},
		{
			"invalid request for item",
			"Krusha",
			&list.Message{Message: "unsuccessful adding item Krusha in the list"},
			"newItem already in the list",
		},
	}

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(), grpc.WithContextDialer(fileDialer()))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := list.NewShoppingListClient(conn)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &list.Item{Name: tt.itemName}

			response, err := client.Create(ctx, request)

			if response != nil {
				if response.GetMessage() != tt.res.GetMessage() {
					t.Error("response: expected", tt.res.GetMessage(), "received", response.GetMessage())
				}
			}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Message() != tt.errMsg {
						t.Error("error message: expected", tt.errMsg, "received", er.Message())
					}
				}
			}

		})
	}

}

func TestShoppingListServer_Delete(t *testing.T) {
	tests := []struct {
		name          string
		prevCreateReq []*list.Item
		itemName      string
		res           *list.Message
		errMsg        string
	}{
		{
			"valid request for deleting first item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Krusha",
			&list.Message{Message: "successful deleting item Krusha in the list"},
			"",
		},
		{
			"valid request for deleting inner item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Topka",
			&list.Message{Message: "successful deleting item Topka in the list"},
			"",
		},
		{
			"valid request for deleting last item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Qbylka",
			&list.Message{Message: "successful deleting item Qbylka in the list"},
			"",
		},
		{
			"invalid request for deleting item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Ivan",
			&list.Message{Message: "unsuccessful deleting item Ivan in the list"},
			"no such exstItem in the list",
		},
	}

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(), grpc.WithContextDialer(dialer()))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := list.NewShoppingListClient(conn)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// moje bi tuk e po-dobre da se syzdava nov chist connection vmesto da se slagat item-ite nanovo ili da se
			//dobavi dunciq na servera koqto da izchistva pametta i da se vika predi vseki test
			for _, item := range tt.prevCreateReq {
				client.Create(ctx, item)
			}

			request := &list.Item{Name: tt.itemName}

			response, err := client.Delete(ctx, request)

			if response != nil {
				if response.GetMessage() != tt.res.GetMessage() {
					t.Error("response: expected", tt.res.GetMessage(), "received", response.GetMessage())
				}
			}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Message() != tt.errMsg {
						t.Error("error message: expected", tt.errMsg, "received", er.Message())
					}
				}
			}

		})
	}

}

func TestShoppingListServerWithFile_Delete(t *testing.T) {
	tests := []struct {
		name          string
		prevCreateReq []*list.Item
		itemName      string
		res           *list.Message
		errMsg        string
	}{
		{
			"valid request for deleting first item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Krusha",
			&list.Message{Message: "successful deleting item Krusha in the list"},
			"",
		},
		{
			"valid request for deleting inner item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Topka",
			&list.Message{Message: "successful deleting item Topka in the list"},
			"",
		},
		{
			"valid request for deleting last item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Qbylka",
			&list.Message{Message: "successful deleting item Qbylka in the list"},
			"",
		},
		{
			"invalid request for deleting item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Ivan",
			&list.Message{Message: "unsuccessful deleting item Ivan in the list"},
			"no such exstItem in the list",
		},
	}

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(), grpc.WithContextDialer(fileDialer()))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := list.NewShoppingListClient(conn)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// moje bi tuk e po-dobre da se syzdava nov chist connection vmesto da se slagat item-ite nanovo ili da se
			//dobavi dunciq na servera koqto da izchistva pametta i da se vika predi vseki test
			for _, item := range tt.prevCreateReq {
				client.Create(ctx, item)
			}

			request := &list.Item{Name: tt.itemName}

			response, err := client.Delete(ctx, request)

			if response != nil {
				if response.GetMessage() != tt.res.GetMessage() {
					t.Error("response: expected", tt.res.GetMessage(), "received", response.GetMessage())
				}
			}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Message() != tt.errMsg {
						t.Error("error message: expected", tt.errMsg, "received", er.Message())
					}
				}
			}

		})
	}

}

func TestShoppingListServerWithFile_CreateAndDelete(t *testing.T) {
	tests := []struct {
		name          string
		prevCreateReq []*list.Item
		itemName      string
		res           []*list.Message
		errMsg        string
	}{
		{
			"valid request for creating after deleting only item",
			[]*list.Item{
				{Name: "Krusha"},
			},
			"Krusha",
			[]*list.Message{
				&list.Message{Message: "successful deleting item Krusha in the list"},
				&list.Message{Message: "successful adding item Krusha in the list"},
			},
			"",
		},
		{
			"valid request for creating after deleting inner item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Topka",
			[]*list.Message{
				&list.Message{Message: "successful deleting item Topka in the list"},
				&list.Message{Message: "successful adding item Topka in the list"},
			},
			"",
		},
		{
			"valid request for creating after deleting last item",
			[]*list.Item{
				{Name: "Krusha"},
				{Name: "Topka"},
				{Name: "Qbylka"},
			},
			"Qbylka",
			[]*list.Message{
				&list.Message{Message: "successful deleting item Qbylka in the list"},
				&list.Message{Message: "successful adding item Qbylka in the list"},
			},
			"",
		},
	}

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(), grpc.WithContextDialer(fileDialer()))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := list.NewShoppingListClient(conn)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// moje bi tuk e po-dobre da se syzdava nov chist connection vmesto da se slagat item-ite nanovo ili da se
			//dobavi dunciq na servera koqto da izchistva pametta i da se vika predi vseki test
			for _, item := range tt.prevCreateReq {
				client.Create(ctx, item)
			}

			deleteRequest := &list.Item{Name: tt.itemName}

			deleteResponse, err := client.Delete(ctx, deleteRequest)

			if deleteResponse != nil {
				if deleteResponse.GetMessage() != tt.res[0].GetMessage() {
					t.Error("delete response: expected", tt.res[0].GetMessage(), "received", deleteResponse.GetMessage())
				}
			}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Message() != tt.errMsg {
						t.Error("error message: expected", tt.errMsg, "received", er.Message())
					}
				}
			}

			createRequest := &list.Item{Name: tt.itemName}

			createResponse, err := client.Create(ctx, createRequest)

			if createResponse != nil {
				if createResponse.GetMessage() != tt.res[1].GetMessage() {
					t.Error("create response: expected", tt.res[1].GetMessage(), "received", createResponse.GetMessage())
				}
			}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Message() != tt.errMsg {
						t.Error("error message: expected", tt.errMsg, "received", er.Message())
					}
				}
			}

		})
	}
}
