package storage

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type Storage interface {
	Add(string) error
	Delete(string) error
	List() []string
}

type FileStorage struct {
	file *os.File
}

type InMemStorage struct {
	items []string
}

func NewInMemStorage() *InMemStorage {
	return &InMemStorage{items: nil}
}

func NewFileStorage(fileName string) *FileStorage {
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalf("error while creating file: %v", err)
	}
	return &FileStorage{file: file}
}

func (s *InMemStorage) Add(newItem string) error {
	for _, item := range s.items {
		if item == newItem {
			return fmt.Errorf("newItem already in the list")
		}
	}
	s.items = append(s.items, newItem)
	return nil
}

func (s *InMemStorage) Delete(exstItem string) error {
	var flag bool
	for i, item := range s.items {
		if item == exstItem {
			s.items = append(s.items[:i], s.items[i+1:]...)
			flag = true
		}
	}
	if !flag {
		return fmt.Errorf("no such exstItem in the list")
	}
	return nil
}

func (s *InMemStorage) List() []string {
	return s.items
}

func (s *FileStorage) Add(newItem string) error {
	data, err := ioutil.ReadFile(s.file.Name())
	if err != nil {
		return err
	}

	items := strings.Split(string(data), ",")

	for _, item := range items {
		if item == newItem {
			return fmt.Errorf("newItem already in the list")
		}
	}

	_, err = s.file.Write([]byte(newItem + ","))
	return nil
}

func (s *FileStorage) Delete(exstItem string) error {
	data, err := ioutil.ReadFile(s.file.Name())
	if err != nil {
		return err
	}

	items := strings.Split(string(data), ",")
	var flag bool = false

	for i, item := range items {
		if item == exstItem {
			items = append(items[:i], items[i+1:]...)
			flag = true
			break
		}
	}

	err = s.file.Truncate(0)
	if err != nil {
		return err
	}

	_, err = s.file.Seek(0, 0)
	if err != nil {
		return err
	}

	_, err = s.file.Write([]byte(strings.Join(items, ",")))
	if err != nil {
		return err
	}

	if !flag {
		return fmt.Errorf("no such exstItem in the list")
	}
	return nil
}

func (s *FileStorage) List() []string {
	data, err := ioutil.ReadFile(s.file.Name())
	if err != nil {
		log.Fatal(err)
	}

	items := strings.Split(string(data), ",")

	return items
}
