package server

import (
	"context"
	"fmt"
	"gitlab.com/gppopoff/simplegrpcapplication/list"
	"gitlab.com/gppopoff/simplegrpcapplication/server/storage"
	"log"
)

type ShoppingListServer struct {
	list.UnimplementedShoppingListServer
	storage storage.Storage
}

func NewInMemShoppingListServer() *ShoppingListServer {
	return &ShoppingListServer{
		UnimplementedShoppingListServer: list.UnimplementedShoppingListServer{},
		storage:                         storage.NewInMemStorage(),
	}
}

func NewFileShoppingListServer(fileName string) *ShoppingListServer {
	return &ShoppingListServer{
		UnimplementedShoppingListServer: list.UnimplementedShoppingListServer{},
		storage:                         storage.NewFileStorage(fileName),
	}
}

func (s *ShoppingListServer) Create(ctn context.Context, in *list.Item) (*list.Message, error) {
	log.Printf("Receive create request from client for item: %s", in.GetName())
	err := s.storage.Add(in.GetName())
	if err != nil {
		message := fmt.Sprintf("unsuccessful adding item %s in the list", in.GetName())
		return &list.Message{Message: message}, err
	}
	message := fmt.Sprintf("successful adding item %s in the list", in.GetName())
	return &list.Message{Message: message}, nil
}

func (s *ShoppingListServer) Delete(ctn context.Context, in *list.Item) (*list.Message, error) {
	log.Printf("Receive delete request from client for item: %s", in.GetName())
	err := s.storage.Delete(in.GetName())
	if err != nil {
		message := fmt.Sprintf("unsuccessful deleting item %s in the list", in.GetName())
		return &list.Message{Message: message}, err
	}
	message := fmt.Sprintf("successful deleting item %s in the list", in.GetName())
	return &list.Message{Message: message}, nil
}

func (s *ShoppingListServer) List(item *list.Item, stream list.ShoppingList_ListServer) error {
	log.Printf("Receive list request from client")
	for _, itemName := range s.storage.List() {
		if err := stream.Send(&list.Item{Name: itemName}); err != nil {
			return err
		}
	}
	return nil
}
